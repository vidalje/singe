# SINGE

is a solver for Spherical INertia-Gravity Eigenmodes, written in Python 3 by Dr Jérémie VIDAL (https://sites.google.com/view/jvidalhome/).

SINGE is distributed under the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible) located in the LICENSE file.
                  
                        / _,\
                        \_\
             ,,,,    _,_)  #      /)
            (= =)D__/    __/     //
           C/^__)/     _(    ___//
             \_,/  -.   '-._/,--'
       _\\_,  /           -//.
        \_ \_/  -,._ _     ) )
          \/    /    )    / /
          \-__,/    (    ( (
                     \.__,-)\_
                      )\_ / -(
                     / -(////
                    ////


FEATURES:
---------

SINGE is written in **Python** 3. It solves the rotating, double-diffusive Boussinesq equations for viscous fluids, which allows you to computes in full **spheres** or spherical **shells**:

* **inertial** and **inertia-gravito modes** in the mantle frame of reference,
* linear **onset** of **Boussinesq convection** with **double-diffusive** effects.
* various boundary conditions are implemented (**no slip** / **stress-free** for the velocity field, **Dirichlet** or **Neumann** conditions for the scalar fields).

SINGE  It uses a **pseudo-spectral** approach in spherical geometry.
The velocity field is projetcted onto poloidal and toroidal scalars, which are expanded on spherical harmonics in the angular directions and finite differences on an irregular mesh in the radial direction.

SINGE relies on other open-source libraries: 

- [SHTns](https://bitbucket.org/nschaeff/shtns) to compute spherical harmonics,
- [PETSc](http://www.mcs.anl.gov/petsc/) and [SLEPc](http://www.grycap.upv.es/slepc/) to solve the eigenvalue problem in parallel (with MPI).


Two research **papers** have been published:

- [Quasi-geostrophic modes in the Earth's fluid core with an outer stably stratified layer](http://dx.doi.org/10.1093/gji/ggv282),
- [Rotating double-diffusive convection in stably stratified planetary cores](http://dx.doi.org/10.1093/gji/ggv282).

USING SINGE:
------------

- Download the source code (as a .zip archive) in the `Downloads` tab.

- If you use SINGE for research work, please **cite the two papers** (and also the paper related to SHTns):

		@article{vidal2015,
		  title={Quasi-geostrophic modes in the Earth's fluid core with an outer stably stratified layer},
		  author={Vidal, J{\'e}r{\'e}mie and Schaeffer, Nathana{\"e}l},
		  journal={Geophysical Journal International},
		  doi={10.1093/gji/ggv282},
		  volume={202}, number={3}, pages={2182--2193},
		  year={2015},
		}
		
		@article{monville2019rotating,
		  title={Rotating double-diffusive convection in stably stratified planetary cores},
		  author={Monville, R{\'e}my and Vidal, J{\'e}r{\'e}mie and C{\'e}bron, David and Schaeffer, Nathana{\"e}l},
		  journal={Geophys. Journal International},
		  volume={219},
		  number={Supplement 1},
		  pages={S195--S218},
		  year={2019},
		  publisher={Oxford University Press},
		  }

QUICK INSTALL:
--------------

- Install a **Python 3** environment (e.g. Anaconda)

- [Download SHTns](https://bitbucket.org/nschaeff/shtns/downloads) and install it with the Python interface.

		./configure --enable-python --disable-openmp
		make
		python setup.py install --user

- [Download PETSc](http://www.mcs.anl.gov/petsc/download/index.html) and install it, using complex arithmetic.
  Note that several setups can be used, please refer to the PETSc documentation. Here is a typical configuration (tuned for the clusters in Grenoble)
  
		./configure --CFLAGS="-O3 -march=native -ffast-math" --FFLAGS="-O3 -march=native -ffast-math" --LDFLAGS="-O3" --with-scalar-type=complex --with-mpi=1 --with-blacs=1 --download-metis --download-parmetis 
		--with-fortran-kernels=1 --with-debugging=no --with-mumps=1 --download-mumps --with-superlu-dist --download-superlu-dist --with-scalapack=1 --download-scalapack CXXOPTFLAGS="-O3 
		-ffast-math -march=native" --CXXFLAGS="-O3 -march=native -ffast-math"

- [Download SLEPc](http://www.grycap.upv.es/slepc/download/download.htm) and install it:

		./configure
		make

- Install the following python packages: 
  numpy, [mpi4py](https://bitbucket.org/mpi4py), [petsc4py](https://bitbucket.org/petsc/petsc4py),
  [slepc4py](https://bitbucket.org/slepc/slepc4py).

- For plotting, you may find convenient to use the `xspp` post-processing program
  (part of the [XSHELLS](https://bitbucket.org/nschaeff/xshells) code), although the `pyxshells.py`
  module included in SINGE can do the job.
  You will probably also need matplotlib or one of the following software: matplotlib, octave, matlab, paraview.
